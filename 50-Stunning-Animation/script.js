gsap
  .timeline({
    scrollTrigger: {
      trigger: "section",
      start: "center center",
      end: "bottom top",
      scrub: true,
      marker: false,
      pin: true,
    },
  })
  .to(".text", { scale: 0 })
  .from(".box1", { opacity: 0 })
  .from(".box2", { opacity: 0 })
  .from(".box3", { opacity: 0 });
