// const imagemin = require("imagemin");
// const imageminJpegtran = require("imagemin-jpegtran");
// const imageminPngquant = require("imagemin-pngquant");
// (async () => {
//   const files = await imagemin([dirSource + "/*.{jpg,jpeg,png}"], {
//     destination: dirDest,
//     plugins: [
//       imageminJpegtran(),
//       imageminPngquant({
//         quality: [0.6, 0.8],
//       }),
//     ],
//   });

//   console.log(files);
//   //=> [{data: <Buffer 89 50 4e …>, destinationPath: 'build/images/foo.jpg'}, …]
// })();
const fs = require("fs");
const path = require("path");
const naturalCompare = require("string-natural-compare");
const compress_images = require("compress-images");
// const dirSource = "E:/TruyenOnline-GetNew/9__Dauanrongthieng/New/";
// const dirDest = "E:/TruyenOnline-GetNew/9__Dauanrongthieng/Out/";
const dirSource = "D:/Hoadv/Truyenvn/xxxxx/New/";
const dirDest = "D:/Hoadv/Truyenvn/xxxxx/Out/";

let files = fs.readdirSync(dirSource);
files.sort((a, b) => naturalCompare(a, b));
// console.log(files);
for (let i = 0; i < files.length; i++) {
  // for (let i = 0; i < 1; i++) {
  // let dirname = createFolder(tap);
  let index = files[i].split("-")[1];
  // let index = files[i].split("_")[0];
  let dirFolder = dirSource + files[i];

  let item = fs.readdirSync(dirFolder);
  item.sort((a, b) => {
    return a.split(".")[0] - b.split(".")[0];
  });

  let INPUT_path_to_your_images =
    dirSource + files[i] + "/*.{jpg,JPG,jpeg,JPEG,png,svg,gif}";

  // let newDest = dirDest + "tap-" + index;
  // if (!fs.existsSync(newDest)) fs.mkdirSync(newDest);
  // console.log(INPUT_path_to_your_images);
  let OUTPUT_path = dirDest + "tap-" + index + "/";
  // console.log(OUTPUT_path);
  compress_images(
    INPUT_path_to_your_images,
    OUTPUT_path,
    { compress_force: false, statistic: true, autoupdate: true },
    false,
    { jpg: { engine: "webp", command: false } },
    { png: { engine: "webp", command: false } },
    { svg: { engine: "webp", command: false } },
    { gif: { engine: "webp", command: false } },
    // { jpg: { engine: "mozjpeg", command: ["-quality", "60"] } },
    // { png: { engine: "pngquant", command: ["--quality=20-50", "-o"] } },
    // { svg: { engine: "svgo", command: "--multipass" } },
    // { gif: { engine: "gifsicle", command: ["--colors", "64", "--use-col=web"] } },
    function (error, completed, statistic) {
      if (error) console.log(error);
    }
  );
  // console.log("Done" + files[i]);
}
// function createFolder(folder) {
//   // let dirname = "tap-" + tap;
//   try {
//     fs.mkdirSync(folder, 0o776);
//   } catch (error) {
//     // createFolder(dirname);
//     console.log("Trung thu muc da tao");
//   }
//   return folder;
// }
// let INPUT_path_to_your_images =
//   dirSource + "/*.{jpg,JPG,jpeg,JPEG,png,svg,gif}";
// let OUTPUT_path = dirDest + "/";
