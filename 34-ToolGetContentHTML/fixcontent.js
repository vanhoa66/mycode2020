const mongoose = require("mongoose");
const epub = require("epub-gen");
const Chap = require("./models/chap.model");
const Story = require("./models/story.model");

const MONGO_URL = "mongodb://localhost:27017/toolget";
mongoose.set("useCreateIndex", true);
mongoose
  .connect(MONGO_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .catch((err) => {
    throw err;
  });
fixtheloai();
async function fixtheloai() {
  try {
    const idStory = "5faca9159444ec1ebc5eb5cd";
    let story = await Story.findById(idStory);
    let chap = await Chap.find({ story: story._id });
    let arrContent = [];
    // chap.forEach(async (c) => {
    for (let i = 1606; i < chap.length; i++) {
      let newContent = chap[i].content
        .replace(/<img([\w\W]+?)>/gi, "")
        .replace(/<a href[\s\S]+?>?<\/a>/gi, "")
        .replace(/<br><br>/gi, "<br>");
      let strP = "<p>";
      let newData = strP.concat(newContent, "</p>");
      let item = {
        title: chap[i].name,
        data: newData,
      };
      arrContent.push(item);
    }
    // console.log(arrContent[0]);
    const options = {
      title: "Toàn chức cao thủ",
      author: "Van Hoa",
      output: "./toan-chuc-cao-thu-continue.epub",
      css: ` * { font-family: 'Droid Serif'; } p { line-height: 1.5em; }`,
      fonts: ["./droid-serif/DroidSerif-Regular.ttf"],
      content: arrContent,

      // [
      // {
      //   title: "Chapter 1: Loomings",
      //   data: dulieu,
      // },
      // ],
    };
    new epub(options).promise.then(() => console.log("Done"));
    // await Chap.find(
    //   { story: story._id, content: new RegExp(/<a href[\s\S]+?>?<\/a>/, "gi") },
    //   function (err, doc) {
    //     //     // console.log(doc.length);
    //     //     // console.log(doc);
    //     if (doc.length > 0) {
    //       doc.forEach(async (d) => {
    //         d.content.replace(/<a href[\s\S]+?>?<\/a>/gi, "");
    //         arrContent.push(d.name);
    //       });
    //     }
    //   }
    // );
    // console.log(arrContent);
    //         // console.log(newContent);
    //         const updateChap = await newChap.updateOne(
    //           { _id: d._id },
    //           // { new: true },
    //           {
    //             $set: {
    //               content: newContent,
    //             },
    //           }
    //         );
    //         console.log("Done " + d._id);
    //       });
    //     }
    //   }
    // );
  } catch (errors) {
    console.log("er");
  }
  // console.log("Done All ");
}
